//
//  DetailsViewModel.swift
//  Recruitment-iOS
//
//  Created by Arkadiusz Marszałek on 04/06/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

class DetailsViewModel {
    private let networkManager: NetworkManager
    private let id: String
    
    init(networkManager: NetworkManager, id: String) {
        self.networkManager = networkManager
        self.id = id
    }
    
    func getItem(completion: @escaping (ItemDetailsViewModel?) -> ()) {
        networkManager.download(item: id) { item in
            if let item = item?.data {
                completion(ItemDetailsViewModel(item: item))
            } else {
                completion(nil)
            }
        }
    }
}
