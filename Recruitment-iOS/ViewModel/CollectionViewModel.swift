//
//  CollectionViewModel.swift
//  Recruitment-iOS
//
//  Created by Arkadiusz Marszałek on 04/06/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

class CollectionViewModel {
    private(set) var items = [ItemViewModel]()
    private let networkManager: NetworkManager
    
    init(networkManager: NetworkManager) {
        self.networkManager = networkManager
    }
    
    func getItems(completion: @escaping () -> ()) {
        networkManager.downloadItems { [weak self] items in
            self?.items = items?.data.map { ItemViewModel(item: $0) } ?? []
            completion()
        }
    }
    
    func getDetails(for id: Int) -> DetailsViewModel {
        return DetailsViewModel(networkManager: networkManager, id: items[id].id)
    }
}
