//
//  UIColor+Name.swift
//  Recruitment-iOS
//
//  Created by Arkadiusz Marszałek on 04/06/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import UIKit

extension UIColor {
    class func color(named: String) -> UIColor {
        let dict: [String: UIColor] = ["Red": .red,
                                       "Green": .green,
                                       "Blue": .blue,
                                       "Yellow": .yellow,
                                       "Purple": .purple]
        return dict[named] ?? .black
    }
}
