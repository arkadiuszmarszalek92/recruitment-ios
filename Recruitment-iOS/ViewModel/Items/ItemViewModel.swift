//
//  ItemViewModel.swift
//  Recruitment-iOS
//
//  Created by Arkadiusz Marszałek on 04/06/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import UIKit

class ItemViewModel {
    private let item: Items.Data
    
    var id: String {
        return item.id
    }
    
    var color: UIColor {
        return UIColor.color(named: item.attributes.color)
    }
    
    var text: String {
        return item.attributes.name
    }
    
    var preview: String {
        return item.attributes.preview
    }
    
    var title: String {
        return prepareTitle()
    }
    
    init(item: Items.Data) {
        self.item = item
    }
    
    private func prepareTitle() -> String {
        let characters = item.attributes.name.enumerated().map { index, letter -> String.Element in
            let even = index % 2 == 0
            return even ? Character(letter.uppercased()) : Character(letter.lowercased())
        }
        return String(characters)
    }
}
