//
//  ItemDetailsViewModel.swift
//  Recruitment-iOS
//
//  Created by Arkadiusz Marszałek on 04/06/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import UIKit

class ItemDetailsViewModel {
    private let item: Item.Data
    
    var text: String {
        return item.attributes.desc
    }
    
    init(item: Item.Data) {
        self.item = item
    }
}
