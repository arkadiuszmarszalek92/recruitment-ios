//
//  Storyboard.swift
//  Recruitment-iOS
//
//  Created by Arkadiusz Marszałek on 04/06/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import Foundation

struct Storyboard {
    struct Main {
        struct View {
            static let tableViewCell = "TableViewCell"
            static let collectionViewCell = "CollectionViewCell"
        }
        
        struct Segue {
            static let showTableDetails = "ShowTableDetails"
            static let showCollectionDetails = "ShowCollectionDetails"
        }
    }
}
