//
//  ItemCollectionViewController.swift
//  Recruitment-iOS
//
//  Created by Arkadiusz Marszałek on 04/06/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import UIKit

class ItemCollectionViewController: UICollectionViewController {
    var viewModel: CollectionViewModel! = CollectionViewModel(networkManager: NetworkManager.shared)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.getItems { [weak self] in
            DispatchQueue.main.async { [weak self] in
                self?.collectionView.reloadData()
            }
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.items.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Storyboard.Main.View.collectionViewCell, for: indexPath)
        (cell as? ItemCollectionViewCell)?.viewModel = viewModel.items[indexPath.row]
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Storyboard.Main.Segue.showCollectionDetails {
            handleShowDetails(destination: segue.destination, sender: sender)
        }
    }
    
    private func handleShowDetails(destination: UIViewController, sender: Any?) {
        guard
            let detailsViewController = destination as? ItemDetailsViewController,
            let cell = sender as? UICollectionViewCell,
            let indexPath = collectionView.indexPath(for: cell)
            else {
                return
        }
        detailsViewController.viewModel = viewModel.getDetails(for: indexPath.row)
        detailsViewController.navigationItem.title = viewModel.items[indexPath.row].title
        detailsViewController.view.backgroundColor = viewModel.items[indexPath.row].color
    }
}

extension ItemCollectionViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let flow = collectionViewLayout as? UICollectionViewFlowLayout else { return .zero }
        
        let itemsPerRow: CGFloat = 2.0
        let padding = flow.sectionInset.left + flow.sectionInset.right + flow.minimumInteritemSpacing
        let dimension = (collectionView.frame.size.width - padding) / itemsPerRow
        
        return CGSize(width: dimension, height: dimension)
    }
}
