//
//  ItemCollectionViewCell.swift
//  Recruitment-iOS
//
//  Created by Arkadiusz Marszałek on 04/06/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import UIKit

class ItemCollectionViewCell: UICollectionViewCell {
    var viewModel: ItemViewModel! {
        didSet {
            titleLabel.text = viewModel.text
            backgroundColor = viewModel.color
        }
    }
    
    @IBOutlet private weak var titleLabel: UILabel!
}
