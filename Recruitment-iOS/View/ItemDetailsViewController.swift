//
//  ItemDetailsViewController.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

import UIKit

class ItemDetailsViewController: UIViewController {
    var viewModel: DetailsViewModel!
    @IBOutlet private weak var textView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.getItem { [weak self] item in
            DispatchQueue.main.async { [weak self] in
                self?.handle(item: item)
            }
        }
    }
    
    private func handle(item: ItemDetailsViewModel?) {
        textView.text = item?.text
    }
}
