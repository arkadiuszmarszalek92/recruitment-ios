//
//  ItemTableViewController.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

import UIKit

class ItemTableViewController: UITableViewController {
    var viewModel: CollectionViewModel! = CollectionViewModel(networkManager: NetworkManager.shared)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.getItems { [weak self] in
            DispatchQueue.main.async { [weak self] in
                self?.tableView.reloadData()
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.items.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Storyboard.Main.View.tableViewCell, for: indexPath)
        let itemViewModel = viewModel.items[indexPath.row]
        cell.backgroundColor = itemViewModel.color
        (cell as? ItemTableViewCell)?.viewModel = itemViewModel
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Storyboard.Main.Segue.showTableDetails {
            handleShowDetails(destination: segue.destination, sender: sender)
        }
    }
    
    private func handleShowDetails(destination: UIViewController, sender: Any?) {
        guard
            let detailsViewController = destination as? ItemDetailsViewController,
            let cell = sender as? UITableViewCell,
            let indexPath = tableView.indexPath(for: cell)
        else {
            return
        }
        detailsViewController.viewModel = viewModel.getDetails(for: indexPath.row)
        detailsViewController.navigationItem.title = viewModel.items[indexPath.row].title
        detailsViewController.view.backgroundColor = viewModel.items[indexPath.row].color
    }
}
