//
//  ItemTableViewCell.swift
//  Recruitment-iOS
//
//  Created by Arkadiusz Marszałek on 05/06/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import UIKit

class ItemTableViewCell: UITableViewCell {
    var viewModel: ItemViewModel! {
        didSet {
            titleLabel.text = viewModel.text
            previewLabel.text = viewModel.preview
            backgroundColor = viewModel.color
        }
    }
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var previewLabel: UILabel!
}
