//
//  Item.swift
//  Recruitment-iOS
//
//  Created by Arkadiusz Marszałek on 04/06/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

class Item: Codable {
    let data: Data
    
    struct Data: Codable {
        let id: String
        let type: String
        let attributes: Attributes
    }
    
    struct Attributes: Codable {
        let name: String
        let color: String
        let desc: String
    }
}
