//
//  NetworkManager.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

import Foundation

protocol RequestHandler {
    func request(file: String, completion: @escaping ((Dictionary<String, AnyObject>) -> Void))
}

class NetworkManager {
    static let shared = NetworkManager()
    private let requestHandler: RequestHandler
    
    init(requestHandler: RequestHandler = JsonRequestHandler()) {
        self.requestHandler = requestHandler
    }
    
    func downloadItems(completion: @escaping (Items?) -> ()) {
        requestHandler.request(file: "Items.json") {
            completion(Items(from: $0))
        }
    }
    
    func download(item: String, completion: @escaping (Item?) -> ()) {
        requestHandler.request(file: "Item\(item).json") {
            completion(Item(from: $0))
        }
    }
}

private class JsonRequestHandler: RequestHandler {
    func request(file: String, completion: @escaping ((Dictionary<String, AnyObject>) -> Void)) {
        request(filename: file, completionBlock: completion)
    }
    
    private func request(filename:String, completionBlock:@escaping (Dictionary<String, AnyObject>) -> Void) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            if let dictionary = JSONParser.jsonFromFilename(filename) {
                completionBlock(dictionary)
            } else {
                completionBlock([:])
            }
        }
    }
}
