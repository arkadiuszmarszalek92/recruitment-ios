//
//  Decodable+Dictionary.swift
//  Recruitment-iOS
//
//  Created by Arkadiusz Marszałek on 04/06/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import Foundation

extension Decodable {
    init?(from dictionary: Dictionary<AnyHashable, Any>) {
        guard let data = try? JSONSerialization.data(withJSONObject: dictionary) else {
            return nil
        }
        
        guard let object = try? JSONDecoder().decode(Self.self, from: data) else {
            return nil
        }
        
        self = object
    }
}
