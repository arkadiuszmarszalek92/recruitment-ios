//
//  MockRequestHandler.swift
//  Recruitment-iOSTests
//
//  Created by Arkadiusz Marszałek on 05/06/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import Foundation
@testable import Recruitment_iOS

class MockRequestHandler: RequestHandler {
    private(set) var file: String?
    
    func request(file: String, completion: @escaping ((Dictionary<String, AnyObject>) -> Void)) {
        self.file = file
        DispatchQueue.global(qos: .background).async {
            completion([:])
        }
    }
}
