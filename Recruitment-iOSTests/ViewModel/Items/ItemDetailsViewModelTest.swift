//
//  ItemDetailsViewModelTest.swift
//  Recruitment-iOSTests
//
//  Created by Arkadiusz Marszałek on 05/06/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import XCTest
@testable import Recruitment_iOS

class ItemDetailsViewModelTest: XCTestCase {
    func testShouldReturnTextFromItemDescAttribute() {
        let itemAttributes = Item.Attributes(name: "test_name", color: "test_color", desc: "test_desc")
        let itemData = Item.Data(id: "test_id", type: "test_type", attributes: itemAttributes)
        
        let testObject = ItemDetailsViewModel(item: itemData)
        
        XCTAssertEqual(testObject.text, "test_desc")
    }
}
