//
//  ItemViewModelTest.swift
//  Recruitment-iOSTests
//
//  Created by Arkadiusz Marszałek on 05/06/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import XCTest
@testable import Recruitment_iOS

class ItemViewModelTest: XCTestCase {
    var testObject: ItemViewModel!
    
    override func setUp() {
        super.setUp()
        testObject = ItemViewModel(item: prepareItemsData())
    }
    
    override func tearDown() {
        testObject = nil
        super.tearDown()
    }
    
    func testShouldReturnIdFromItemId() {
        XCTAssertEqual(testObject.id, "test_id")
    }
    
    func testShouldReturnColorFromItemColorAttribute() {
        XCTAssertEqual(testObject.color, .black)
    }
    
    func testShouldReturnTextFromNameAttribute() {
        XCTAssertEqual(testObject.text, "test_name")
    }
    
    func testShouldReturnPreviewFromPreviewAttribute() {
        XCTAssertEqual(testObject.preview, "test_preview")
    }
    
    func testShouldPrepareTitleFromNameAttribute() {
        XCTAssertEqual(testObject.title, "TeSt_nAmE")
    }
    
    private func prepareItemsData() -> Items.Data {
        let itemsAttributes = Items.Attributes(name: "test_name", preview: "test_preview", color: "test_color")
        return Items.Data(id: "test_id", type: "test_type", attributes: itemsAttributes)
    }
}
