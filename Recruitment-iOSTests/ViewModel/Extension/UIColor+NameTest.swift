//
//  UIColor+NameTest.swift
//  Recruitment-iOSTests
//
//  Created by Arkadiusz Marszałek on 05/06/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import XCTest
@testable import Recruitment_iOS

class UIColor_NameTest: XCTestCase {
    func testShouldReturnColorsForNames() {
        let colorNames = ["Red", "Green", "Blue", "Yellow", "Purple"]
        
        let colors = colorNames.map { UIColor.color(named: $0) }
        
        XCTAssertEqual(colors.count, colorNames.count)
        XCTAssertEqual(colors[0], .red)
        XCTAssertEqual(colors[1], .green)
        XCTAssertEqual(colors[2], .blue)
        XCTAssertEqual(colors[3], .yellow)
        XCTAssertEqual(colors[4], .purple)
    }
    
    func testShouldReturnBlackIfUnknownName() {
        let colorName = "Unknown"
        
        let color = UIColor.color(named: colorName)
        
        XCTAssertEqual(color, .black)
    }
}
