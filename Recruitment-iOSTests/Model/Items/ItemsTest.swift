//
//  ItemsTest.swift
//  Recruitment-iOSTests
//
//  Created by Arkadiusz Marszałek on 05/06/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import XCTest
@testable import Recruitment_iOS

class ItemsTest: XCTestCase {
    func testShouldParseItemsFromDictionary() {
        let json = prepareJSON()
        
        let items = Items(from: json)
        
        XCTAssertNotNil(items)
        XCTAssertEqual(items?.data.count, 2)
        XCTAssertEqual(items?.data.first?.id, "test_id_1")
        XCTAssertEqual(items?.data.first?.type, "test_type_1")
        XCTAssertEqual(items?.data.first?.attributes.name, "test_name_1")
        XCTAssertEqual(items?.data.first?.attributes.preview, "test_color_1")
        XCTAssertEqual(items?.data.first?.attributes.color, "test_desc_1")
        
        XCTAssertEqual(items?.data[1].id, "test_id_2")
        XCTAssertEqual(items?.data[1].type, "test_type_2")
        XCTAssertEqual(items?.data[1].attributes.name, "test_name_2")
        XCTAssertEqual(items?.data[1].attributes.preview, "test_color_2")
        XCTAssertEqual(items?.data[1].attributes.color, "test_desc_2")
    }
    
    func testShouldNotParseItemsFromEmptyDictionary() {
        let items = Items(from: [:])
        
        XCTAssertNil(items)
    }
    
    func testShouldNotParseItemFromDictionaryWithWrongValues() {
        let wrongValue = String(bytes: [0xD8, 0x00], encoding: .utf16BigEndian) as Any
        
        let items = Items(from: ["test": wrongValue])
        
        XCTAssertNil(items)
    }
    
    private func prepareJSON() -> Dictionary<AnyHashable, Any> {
        return [
            "data": [
                [
                    "id": "test_id_1",
                    "type": "test_type_1",
                    "attributes": [
                        "name": "test_name_1",
                        "preview": "test_color_1",
                        "color": "test_desc_1"
                    ]
                ],
                [
                    "id": "test_id_2",
                    "type": "test_type_2",
                    "attributes": [
                        "name": "test_name_2",
                        "preview": "test_color_2",
                        "color": "test_desc_2"
                    ]
                ]
            ]
        ]
    }
}
