//
//  Recruitment_iOSTests.swift
//  Recruitment-iOSTests
//
//  Created by Arkadiusz Marszałek on 05/06/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import XCTest
@testable import Recruitment_iOS

class ItemTest: XCTestCase {
    func testShouldParseItemFromDictionary() {
        let json = prepareJSON()
        
        let item = Item(from: json)
        
        XCTAssertNotNil(item)
        XCTAssertEqual(item?.data.id, "test_id")
        XCTAssertEqual(item?.data.type, "test_type")
        XCTAssertEqual(item?.data.attributes.name, "test_name")
        XCTAssertEqual(item?.data.attributes.color, "test_color")
        XCTAssertEqual(item?.data.attributes.desc, "test_desc")
    }
    
    func testShouldNotParseItemFromEmptyDictionary() {
        let item = Item(from: [:])
        
        XCTAssertNil(item)
    }
    
    func testShouldNotParseItemFromDictionaryWithWrongValues() {
        let wrongValue = String(bytes: [0xD8, 0x00], encoding: .utf16BigEndian) as Any
        
        let item = Item(from: ["test": wrongValue])
        
        XCTAssertNil(item)
    }
    
    private func prepareJSON() -> Dictionary<AnyHashable, Any> {
        return [
            "data": [
                "id": "test_id",
                "type": "test_type",
                "attributes": [
                    "name": "test_name",
                    "color": "test_color",
                    "desc": "test_desc"
                ]
            ]
        ]
    }
}
