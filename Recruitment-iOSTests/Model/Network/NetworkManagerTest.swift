//
//  NetworkManagerTest.swift
//  Recruitment-iOSTests
//
//  Created by Arkadiusz Marszałek on 05/06/2019.
//  Copyright © 2019 Untitled Kingdom. All rights reserved.
//

import XCTest
@testable import Recruitment_iOS

class NetworkManagerTest: XCTestCase {
    var testObject: NetworkManager!
    var mockRequestHandler: MockRequestHandler!
    
    override func setUp() {
        super.setUp()
        mockRequestHandler = MockRequestHandler()
        testObject = NetworkManager(requestHandler: mockRequestHandler)
    }
    
    override func tearDown() {
        testObject = nil
        mockRequestHandler = nil
        super.tearDown()
    }
    
    func testShouldStartRequestForItem() {
        let item = "test_item"
        
        testObject.download(item: item) { _ in }
        
        XCTAssertEqual(mockRequestHandler.file, "Item\(item).json")
    }
    
    func testShouldCallCompletionOnFinishedRequestForItem() {
        let expectCompleted = expectation(description: "Completed")
        
        testObject.download(item: "test_item") { item in
            expectCompleted.fulfill()
        }
        
        waitForExpectations(timeout: 0.5, handler: nil)
    }
    
    func testShouldStartRequestForItems() {
        testObject.downloadItems() { _ in }
        
        XCTAssertEqual(mockRequestHandler.file, "Items.json")
    }
    
    func testShouldCallCompletionOnFinishedRequestForItems() {
        let expectCompleted = expectation(description: "Completed")
        
        testObject.downloadItems { _ in
            expectCompleted.fulfill()
        }
        
        waitForExpectations(timeout: 0.5, handler: nil)
    }
}
